/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.text.StrSubstitutor;
import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.Function;

public class BuildURI implements Function {

	@Override
	public String getURI() {
		return SPIF.BUILD_URI_FUNCTION.toString();
	}

	@Override
	public Value evaluate(ValueFactory valueFactory, Value... args) throws ValueExprEvaluationException {
		if (args.length < 1) {
			throw new ValueExprEvaluationException("Incorrect number of arguments");
		}
		if (!(args[0] instanceof Literal)) {
			throw new ValueExprEvaluationException("First argument must be a string");
		}
		Literal s = (Literal) args[0];
		String tmpl = s.getLabel();
		Map<String, String> mappings = new HashMap<String, String>(args.length);
		for (int i=1; i<args.length; i++) {
			mappings.put(Integer.toString(i), args[i].stringValue());
		}
		String newValue = StrSubstitutor.replace(tmpl, mappings, "{?", "}");
		if (tmpl.charAt(0) == '<' && tmpl.charAt(tmpl.length()-1) == '>') {
			return valueFactory.createURI(newValue.substring(1, newValue.length()-1));
		}
		throw new ValueExprEvaluationException("Invalid URI template: "+tmpl);
	}
}
