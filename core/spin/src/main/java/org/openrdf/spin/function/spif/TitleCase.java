/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import org.openrdf.model.vocabulary.SPIF;

public class TitleCase extends AbstractStringReplacer {

	public TitleCase() {
		super(SPIF.TITLE_CASE_FUNCTION.toString());
	}

	@Override
	protected String transform(String s) {
		StringBuilder buf = new StringBuilder(s.length());
		char prev = '\0';
		for (int i=0; i<s.length(); i++) {
			char ch = s.charAt(i);
			if (i == 0 || prev == ' ') {
				buf.append(Character.toUpperCase(ch));
			}
			else {
				buf.append(ch);
			}
			prev = ch;
		}
		return buf.toString();
	}
}
