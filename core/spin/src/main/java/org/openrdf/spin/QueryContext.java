/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin;

import org.openrdf.query.algebra.evaluation.QueryPreparer;



public class QueryContext {
	private static final ThreadLocal<QueryPreparer> queryPreparer = new ThreadLocal<QueryPreparer>();

	public static QueryPreparer getQueryPreparer() 
	{
		return queryPreparer.get();
	}

	public static QueryContext begin(QueryPreparer qp) 
	{
		return new QueryContext(qp);
	}

	private final QueryPreparer previous;

	private QueryContext(QueryPreparer qp) {
		this.previous = queryPreparer.get();
		queryPreparer.set(qp);
	}

	public void end() {
		queryPreparer.remove();
		if(previous != null) {
			queryPreparer.set(previous);
		}
	}
}
