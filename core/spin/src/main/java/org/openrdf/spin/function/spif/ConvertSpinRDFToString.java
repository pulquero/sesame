/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.Function;
import org.openrdf.query.parser.ParsedOperation;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.queryrender.sparql.SPARQLQueryRenderer;
import org.openrdf.spin.SpinParser;
import org.openrdf.spin.function.AbstractSpinFunction;

public class ConvertSpinRDFToString extends AbstractSpinFunction implements Function {

	private SpinParser parser;

	public ConvertSpinRDFToString() {
		super(SPIF.CONVERT_SPIN_RDF_TO_STRING_FUNCTION.stringValue());
	}

	public ConvertSpinRDFToString(SpinParser parser) {
		this();
		this.parser = parser;
	}

	public SpinParser getSpinParser() {
		return parser;
	}

	public void setSpinParser(SpinParser parser) {
		this.parser = parser;
	}

	@Override
	public Value evaluate(ValueFactory valueFactory, Value... args) throws ValueExprEvaluationException {
		if (args.length < 1 || args.length > 2) {
			throw new ValueExprEvaluationException("Incorrect number of arguments");
		}
		if (!(args[0] instanceof Resource)) {
			throw new ValueExprEvaluationException("First argument must be the root of a SPIN RDF query");
		}
		if (args.length == 2 && !(args[1] instanceof Literal)) {
			throw new ValueExprEvaluationException("Second argument must be a string");
		}
		Resource q = (Resource) args[0];
		boolean useHtml = (args.length == 2) ? ((Literal) args[1]).booleanValue() : false;
		String sparqlString;
		try {
			ParsedOperation op = parser.parse(q, getCurrentQueryPreparer().getTripleSource());
			sparqlString = new SPARQLQueryRenderer().render((ParsedQuery)op);
		} catch (Exception e) {
			throw new ValueExprEvaluationException(e);
		}
		return valueFactory.createLiteral(sparqlString);
	}
}
