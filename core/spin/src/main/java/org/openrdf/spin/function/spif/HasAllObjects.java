/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import org.openrdf.model.Resource;
import org.openrdf.model.IRI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.BooleanLiteralImpl;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.evaluation.QueryPreparer;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.Function;
import org.openrdf.query.algebra.evaluation.util.Statements;
import org.openrdf.spin.function.AbstractSpinFunction;

import info.aduna.iteration.Iteration;


public class HasAllObjects extends AbstractSpinFunction implements Function {

	public HasAllObjects() {
		super(SPIF.HAS_ALL_OBJECTS_FUNCTION.stringValue());
	}

	@Override
	public Value evaluate(ValueFactory valueFactory, Value... args)
		throws ValueExprEvaluationException
	{
		QueryPreparer qp = getCurrentQueryPreparer();
		if (args.length != 3) {
			throw new ValueExprEvaluationException(String.format("%s requires 3 argument, got %d", getURI(), args.length));
		}
		Resource subj = (Resource) args[0];
		IRI pred = (IRI) args[1];
		Resource list = (Resource) args[2];
		try {
			Iteration<? extends Value, QueryEvaluationException> iter = Statements.list(list, qp.getTripleSource());
			while (iter.hasNext()) {
				Value obj = iter.next();
				if (Statements.single(subj, pred, obj, qp.getTripleSource()) == null) {
					return BooleanLiteralImpl.FALSE;
				}
			}
		} catch (QueryEvaluationException e) {
			throw new ValueExprEvaluationException(e);
		}
		return BooleanLiteralImpl.TRUE;
	}
}
