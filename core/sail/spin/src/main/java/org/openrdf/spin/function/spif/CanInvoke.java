/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import info.aduna.iteration.CloseableIteration;
import info.aduna.iteration.EmptyIteration;
import info.aduna.iteration.Iterations;
import info.aduna.iteration.SingletonIteration;

import org.openrdf.OpenRDFException;
import org.openrdf.model.IRI;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.BooleanLiteralImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.model.vocabulary.SPIN;
import org.openrdf.model.vocabulary.SPL;
import org.openrdf.query.BindingSet;
import org.openrdf.query.Dataset;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.UpdateExecutionException;
import org.openrdf.query.algebra.QueryRoot;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.UpdateExpr;
import org.openrdf.query.algebra.evaluation.AbstractQueryPreparer;
import org.openrdf.query.algebra.evaluation.EvaluationStrategy;
import org.openrdf.query.algebra.evaluation.QueryPreparer;
import org.openrdf.query.algebra.evaluation.TripleSource;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.federation.AbstractFederatedServiceResolver;
import org.openrdf.query.algebra.evaluation.federation.FederatedServiceResolver;
import org.openrdf.query.algebra.evaluation.federation.FederatedServiceResolverImpl;
import org.openrdf.query.algebra.evaluation.function.Function;
import org.openrdf.query.algebra.evaluation.function.FunctionRegistry;
import org.openrdf.query.algebra.evaluation.function.TupleFunctionRegistry;
import org.openrdf.query.algebra.evaluation.impl.BindingAssigner;
import org.openrdf.query.algebra.evaluation.impl.CompareOptimizer;
import org.openrdf.query.algebra.evaluation.impl.ConjunctiveConstraintSplitter;
import org.openrdf.query.algebra.evaluation.impl.ConstantOptimizer;
import org.openrdf.query.algebra.evaluation.impl.DisjunctiveConstraintOptimizer;
import org.openrdf.query.algebra.evaluation.impl.EvaluationStatistics;
import org.openrdf.query.algebra.evaluation.impl.EvaluationStrategyImpl;
import org.openrdf.query.algebra.evaluation.impl.FilterOptimizer;
import org.openrdf.query.algebra.evaluation.impl.IterativeEvaluationOptimizer;
import org.openrdf.query.algebra.evaluation.impl.OrderLimitOptimizer;
import org.openrdf.query.algebra.evaluation.impl.QueryJoinOptimizer;
import org.openrdf.query.algebra.evaluation.impl.QueryModelNormalizer;
import org.openrdf.query.algebra.evaluation.impl.SameTermFilterOptimizer;
import org.openrdf.query.algebra.evaluation.impl.TupleFunctionEvaluationStrategy;
import org.openrdf.query.algebra.evaluation.util.Statements;
import org.openrdf.sail.spin.SpinFunctionInterpreter;
import org.openrdf.sail.spin.SpinInferencing;
import org.openrdf.sail.spin.SpinMagicPropertyInterpreter;
import org.openrdf.spin.Argument;
import org.openrdf.spin.ConstraintViolation;
import org.openrdf.spin.SpinParser;
import org.openrdf.spin.function.AbstractSpinFunction;

public class CanInvoke extends AbstractSpinFunction implements Function {

	private SpinParser parser;

	public CanInvoke() {
		super(SPIF.CAN_INVOKE_FUNCTION.stringValue());
	}

	public CanInvoke(SpinParser parser) {
		this();
		this.parser = parser;
	}

	public SpinParser getSpinParser() {
		return parser;
	}

	public void setSpinParser(SpinParser parser) {
		this.parser = parser;
	}

	@Override
	public Value evaluate(ValueFactory valueFactory, Value... args)
		throws ValueExprEvaluationException
	{
		QueryPreparer qp = getCurrentQueryPreparer();
		final TripleSource qpTripleSource = qp.getTripleSource();
		if (args.length == 0) {
			throw new ValueExprEvaluationException("At least one argument is required");
		}
		if (!(args[0] instanceof IRI)) {
			throw new ValueExprEvaluationException("The first argument must be a function IRI");
		}

		IRI func = (IRI)args[0];

		try {
			Function instanceOfFunc = getFunction("http://spinrdf.org/spl#instanceOf", qpTripleSource,
					FunctionRegistry.getInstance());
			Map<IRI, Argument> funcArgs = parser.parseArguments(func, qpTripleSource);
			List<IRI> funcArgList = SpinParser.orderArguments(funcArgs.keySet());
			final Map<IRI, Value> argValues = new HashMap<IRI, Value>(funcArgList.size());
			for (int i = 0; i < funcArgList.size(); i++) {
				IRI argName = funcArgList.get(i);
				Argument funcArg = funcArgs.get(argName);
				int argIndex = i + 1;
				if (argIndex < args.length) {
					Value argValue = args[argIndex];
					IRI valueType = funcArg.getValueType();
					Value isInstance = instanceOfFunc.evaluate(valueFactory, argValue, valueType);
					if (((Literal)isInstance).booleanValue()) {
						argValues.put(argName, argValue);
					}
					else {
						return BooleanLiteralImpl.FALSE;
					}
				}
				else if (!funcArg.isOptional()) {
					return BooleanLiteralImpl.FALSE;
				}
			}

			/* in order to check any additional constraints we have to create a virtual function instance
			to run them against */
			final Resource funcInstance = qpTripleSource.getValueFactory().createBNode();

			TripleSource tempTripleSource = new TripleSource() {

				private final ValueFactory vf = qpTripleSource.getValueFactory();

				@Override
				public CloseableIteration<? extends Statement, QueryEvaluationException> getStatements(
						Resource subj, IRI pred, Value obj, Resource... contexts)
					throws QueryEvaluationException
				{
					if (funcInstance.equals(subj)) {
						if (pred != null) {
							Value v = argValues.get(pred);
							if (v != null && (obj == null || v.equals(obj))) {
								return new SingletonIteration<Statement, QueryEvaluationException>(
										vf.createStatement(subj, pred, v));
							}
						}

						return new EmptyIteration<Statement, QueryEvaluationException>();
					}
					else {
						return qpTripleSource.getStatements(subj, pred, obj, contexts);
					}
				}

				@Override
				public ValueFactory getValueFactory() {
					return vf;
				}
			};

			QueryPreparer tempQueryPreparer = new AbstractQueryPreparer(tempTripleSource) {

				private final ValueFactory vf = getTripleSource().getValueFactory();

				private final FunctionRegistry functionRegistry = FunctionRegistry.getInstance();

				private final TupleFunctionRegistry tupleFunctionRegistry = TupleFunctionRegistry.getInstance();

				private final AbstractFederatedServiceResolver serviceResolver = new FederatedServiceResolverImpl();

				@Override
				protected CloseableIteration<? extends BindingSet, QueryEvaluationException> evaluate(
						TupleExpr tupleExpr, Dataset dataset, BindingSet bindings, boolean includeInferred,
						int maxExecutionTime)
					throws QueryEvaluationException
				{
					// Clone the tuple expression to allow for more aggressive
					// optimizations
					tupleExpr = tupleExpr.clone();

					if (!(tupleExpr instanceof QueryRoot)) {
						// Add a dummy root node to the tuple expressions to allow the
						// optimizers to modify the actual root node
						tupleExpr = new QueryRoot(tupleExpr);
					}

					new SpinFunctionInterpreter(parser, getTripleSource(), functionRegistry).optimize(tupleExpr,
							dataset, bindings);
					new SpinMagicPropertyInterpreter(parser, getTripleSource(), tupleFunctionRegistry,
							serviceResolver).optimize(tupleExpr, dataset, bindings);

					EvaluationStrategy strategy = new TupleFunctionEvaluationStrategy(
							new EvaluationStrategyImpl(getTripleSource(), dataset, serviceResolver), vf,
							tupleFunctionRegistry);

					// do standard optimizations
					new BindingAssigner().optimize(tupleExpr, dataset, bindings);
					new ConstantOptimizer(strategy).optimize(tupleExpr, dataset, bindings);
					new CompareOptimizer().optimize(tupleExpr, dataset, bindings);
					new ConjunctiveConstraintSplitter().optimize(tupleExpr, dataset, bindings);
					new DisjunctiveConstraintOptimizer().optimize(tupleExpr, dataset, bindings);
					new SameTermFilterOptimizer().optimize(tupleExpr, dataset, bindings);
					new QueryModelNormalizer().optimize(tupleExpr, dataset, bindings);
					new QueryJoinOptimizer(new EvaluationStatistics()).optimize(tupleExpr, dataset, bindings);
					// new SubSelectJoinOptimizer().optimize(tupleExpr, dataset,
					// bindings);
					new IterativeEvaluationOptimizer().optimize(tupleExpr, dataset, bindings);
					new FilterOptimizer().optimize(tupleExpr, dataset, bindings);
					new OrderLimitOptimizer().optimize(tupleExpr, dataset, bindings);

					return strategy.evaluate(tupleExpr, bindings);
				}

				@Override
				protected void execute(UpdateExpr updateExpr, Dataset dataset, BindingSet bindings,
						boolean includeInferred, int maxExecutionTime)
					throws UpdateExecutionException
				{
					throw new UnsupportedOperationException();
				}
			};

			CloseableIteration<? extends Resource, QueryEvaluationException> iter = Statements.getObjectResources(
					func, SPIN.CONSTRAINT_PROPERTY, qpTripleSource);
			try {
				while (iter.hasNext()) {
					Resource constraint = iter.next();
					Set<IRI> constraintTypes = Iterations.asSet(
							Statements.getObjectURIs(constraint, RDF.TYPE, qpTripleSource));
					// skip over argument constraints that we have already checked
					if (!constraintTypes.contains(SPL.ARGUMENT_TEMPLATE)) {
						ConstraintViolation violation = SpinInferencing.checkConstraint(funcInstance, constraint,
								tempQueryPreparer, parser);
						if (violation != null) {
							return BooleanLiteralImpl.FALSE;
						}
					}
				}
			}
			finally {
				iter.close();
			}
		}
		catch (OpenRDFException e) {
			throw new ValueExprEvaluationException(e);
		}

		return BooleanLiteralImpl.TRUE;
	}

	private Function getFunction(String name, TripleSource tripleSource, FunctionRegistry functionRegistry)
		throws OpenRDFException
	{
		Function func = functionRegistry.get(name).orElse(null);
		if (func == null) {
			IRI funcUri = tripleSource.getValueFactory().createIRI(name);
			func = parser.parseFunction(funcUri, tripleSource);
			functionRegistry.add(func);
		}
		return func;
	}
}
