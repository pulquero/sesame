/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.sail.spin.config;

import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.util.ModelException;
import org.openrdf.model.util.Models;
import org.openrdf.sail.config.AbstractDelegatingSailImplConfig;
import org.openrdf.sail.config.SailConfigException;
import org.openrdf.sail.config.SailImplConfig;

public class SpinSailConfig extends AbstractDelegatingSailImplConfig {

	/**
	 * Flag to indicate if the SPIN engine should initialize using the full
	 * deductive closure of the SPIN axioms. This is necessary if the underlying
	 * Sail stack does not provide RDFS inferencing.
	 */
	private boolean axiomClosureNeeded = true;

	public SpinSailConfig() {
		super(SpinSailFactory.SAIL_TYPE);
	}

	public SpinSailConfig(SailImplConfig delegate) {
		super(SpinSailFactory.SAIL_TYPE, delegate);
		if ("openrdf:ForwardChainingRDFSInferencer".equals(delegate.getType())) {
			setAxiomClosureNeeded(false);
		}
	}

	public SpinSailConfig(SailImplConfig delegate, boolean axiomClosureNeeded) {
		super(SpinSailFactory.SAIL_TYPE, delegate);
		setAxiomClosureNeeded(axiomClosureNeeded);
	}

	public boolean isAxiomClosureNeeded() {
		return axiomClosureNeeded;
	}

	public void setAxiomClosureNeeded(boolean axiomClosureNeeded) {
		this.axiomClosureNeeded = axiomClosureNeeded;
	}

	@Override
	public void parse(Model m, Resource implNode)
		throws SailConfigException
	{
		super.parse(m, implNode);

		try {
			Models.objectLiteral(m.filter(implNode, SpinSailSchema.AXIOM_CLOSURE_NEEDED, null)).ifPresent(
					lit -> setAxiomClosureNeeded(lit.booleanValue()));
		}
		catch (ModelException e) {
			throw new SailConfigException(e.getMessage(), e);
		}
	}
}
